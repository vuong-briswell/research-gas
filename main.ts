import 'google-apps-script';
var ssId = "1CTZ4UwTYwCnduIaL9rNObZETUZTK6H30YCeN08Gqquc";
var formId = "1qCeq0qOR6UsFssNn0lntQPjFj5EXvqwQ76sEIseJHvQ";
var wsData = SpreadsheetApp.openById(ssId).getSheetByName("demo");
var from = FormApp.openById(formId);
const main: Function = (): void => {
    inforRequired();
    Fields();
}

const inforRequired :Function = () : void => {
    from.setTitle('Bài kiểm tra trình độ tiếng Anh nội bộ công ty Briswell Việt Nam - Kỳ thi: tháng 04 năm 2020')
    .setDescription(`
    - Bài thi gồm có 2 phần lớn và 7 phần nhỏ:
    　LISTENING TEST
    　　Part I: Picture Description
    　　Part II: Question - Response
    　　Part III: Short Conversations
    　　Part IV: Short Talks
    　READING TEST
    　　Part V: Incomplete Sentences
    　　Part VI: Text Completion
    　　Part VII: Reading Comprehension
    - Thời gian làm bài là 30 phút, tính từ khi nhấn nút Start
    - Điểm được công bố ngay sau khi nộp bài làm`)
    .setConfirmationMessage('Thanks for responding!')
    .setAllowResponseEdits(false);
}

const Fields: Function = () : void => {
    var range = wsData.getDataRange().getValues();
    range.forEach( (question, index) => {
       if (question[1] === 'TEXT') {
           from.addTextItem().setTitle(question[0]).setRequired(true);
       }
       if (question[1] === 'DATETIME') {
         from.addDateItem().setTitle(question[0]).setRequired(true);
       }
       if (question[1] === 'EMAIL') {
       var email = from.addTextItem().setTitle(question[0]).setRequired(true);
        email.setValidation(FormApp.createTextValidation().requireTextIsEmail().build());
       }
       if (question[1] === 'RADIO') {
        var item = from.addMultipleChoiceItem();
        item.setTitle(question[0]).setChoices([
            item.createChoice('Nam'),
            item.createChoice('Nữ')
        ]).showOtherOption(true).setRequired(true);
       }
    });
}
